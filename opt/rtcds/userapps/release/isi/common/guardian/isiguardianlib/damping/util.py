from ezca.ligofilter import LIGOFilterManager

from .. import const as top_const
from .. import util as top_util
from . import const as damp_const

##################################################
# NOTE: the following functions are BLOCKING and will not return until
# all changes have completed their ramps.  This could be multiple
# seconds if ramp=True.  This function should then be used sparingly
# (such as only in INIT).

def clear_damp_filters(ramp=False):
    lfm_damp = LIGOFilterManager(['DAMP_'+dof for dof in top_const.DOF_LIST], ezca)
    if ramp:
        ramp_time = damp_const.DAMPING_CONSTANTS['RAMP_DOWN_TIME']
    else:
        ramp_time = 0
    lfm_damp.ramp_gains(gain_value=0, 
                        ramp_time=ramp_time,
                        wait=True)
    lfm_damp.all_do('all_off', wait=True)


def undamp():
    clear_damp_filters(ramp=True)

##################################################

def check_if_damping_loops_on():
    if top_const.CHAMBER_TYPE == 'HPI':
        return
    filter_channel_names = ['DAMP_'+dof for dof in damp_const.DAMPING_CONSTANTS['ALL_DOF']]
    error_message = top_util.check_if_requested_filter_modules_loaded(filter_channel_names,
                                                             damp_const.DAMPING_CONSTANTS['ONLY_ON_BUTTONS'],
                                                             damp_const.DAMPING_CONSTANTS['FILTER_NAMES'],)
    error_message += top_util.check_if_filters_have_correct_gain(filter_channel_names, damp_const.DAMPING_CONSTANTS['GAIN'])
    return error_message
